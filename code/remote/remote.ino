
#define TRANSMISSION_PIN 4  
#define LEFT A0
#define RIGHT A1
#define SWITCH_MODE_PIN 2
#define MESSAGE_LENGTH 3
#define PID_LED_PIN 12
#define TANK_LED_PIN 8
#define DEBUGNOT
#define DATA_RATE 2000
#include "debug.h"

#include <VirtualWire.h>
#include "tank_mode.h"
#include "pid_mode.h"

volatile bool isTankMode;

void switchMode() {
  isTankMode = !isTankMode;
}

void setup(){
  #ifdef DEBUG
  Serial.begin(9600);
  #endif
	pinMode(TRANSMISSION_PIN, OUTPUT);  
  pinMode(PID_LED_PIN, OUTPUT);
  pinMode(TANK_LED_PIN, OUTPUT);
  pinMode(SWITCH_MODE_PIN, INPUT_PULLUP);   
  attachInterrupt(digitalPinToInterrupt(SWITCH_MODE_PIN), switchMode,
    RISING);
	tank_setup();
  pid_setup();
	vw_set_ptt_inverted(true); 
	vw_set_tx_pin(TRANSMISSION_PIN);
	vw_setup(DATA_RATE);
  isTankMode = true;
}

void loop(){
  printlnd("Step----");
  printd("Mode ");
  if (isTankMode) {
    printlnd("Tank mode");
  } else {
    printlnd("PID mode");
  }
	if (isTankMode) {
    digitalWrite(PID_LED_PIN, LOW);
    digitalWrite(TANK_LED_PIN, HIGH);
		vw_send(tank_get_message(), TANK_MESSAGE_LENGTH);
		vw_wait_tx();
	} else {
    digitalWrite(PID_LED_PIN, HIGH);
    digitalWrite(TANK_LED_PIN, LOW);
		uint8_t* msg = pid_get_message();
		if (msg != NULL) {
      printd("PID message = ");
      printd(msg[0], BIN);
      printlnd();
      int i = 0;
      while (i++ < 10) {
      vw_send(msg, PID_MESSAGE_LENGTH);
      vw_wait_tx();
      }
		}
	}
	delay(50);
}
