#include "debug.h"
#define PID_MESSAGE_LENGTH 3
#define PID_SEND_PIN 3
#define X_AXIS A2
#define Y_AXIS RIGHT

void pid_setup();
uint8_t* pid_get_message();
