#include "pid_mode.h"
#include "debug.h"

unsigned int pid_lowerThreshold = 200;
unsigned int pid_highThreshold = 900;
uint8_t pid_controller[3];

volatile bool pid_should_send = false;

int pid_x_neutral, pid_y_neutral;
bool pid_was_high_left = false;
bool pid_was_high_right = false;
bool pid_was_high_down = false;
bool pid_was_high_up = false;

unsigned int pid_number_of_move_left = 0; 
unsigned int pid_number_of_move_right = 0; 
unsigned int pid_number_of_move_up = 0; 
byte pid_id;

void pid_send() {
	pid_should_send = true;
}

void pid_setup() {
	pinMode(PID_SEND_PIN, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(PID_SEND_PIN), pid_send,
    RISING);
	pid_x_neutral = analogRead(X_AXIS);
	pid_y_neutral = analogRead(Y_AXIS);
  pid_should_send = false;
  pid_id = 0;
}

void pid_reset() {
	pid_was_high_left = false;
	pid_was_high_right = false;
	pid_was_high_down = false;
	pid_was_high_up = false;
}

void pid_reset_count() {
	pid_number_of_move_left = 0;
	pid_number_of_move_right = 0;
	pid_number_of_move_up = 0; 
}

void pid_reset_controller() {
	pid_controller[0] = 0;
	pid_controller[1] = 0;
	pid_controller[2] = 0;
}

uint8_t* pid_get_message() {
	if (pid_should_send) {
		pid_reset_controller();
    printlnd("SENDING MESSAGE!!!!!!");
		pid_controller[0] = 0b100;
    pid_controller[0] = pid_controller[0] | (pid_id << 4);
    pid_id += 1;
		int diff = pid_number_of_move_right - pid_number_of_move_left;
		if (diff > 0) {
			pid_controller[1] = (uint8_t)diff;
		} else {
			pid_controller[0] = pid_controller[0] | 1;
			pid_controller[1] = (uint8_t) (diff * -1);
		}

		diff = pid_number_of_move_up;
		if (diff > 0) {
			pid_controller[2] = (uint8_t)diff;
		} else {
			pid_controller[0] = pid_controller[0] | 0b10;
			pid_controller[2] = 0;
		}
		pid_should_send = false;
		pid_reset();
		pid_reset_count();
    printlnd(pid_controller[0], BIN);
    printlnd(pid_controller[1], DEC);
    printlnd(pid_controller[2], DEC);
		return pid_controller;
	} else {
		//store reading
		unsigned int xReading = analogRead(X_AXIS);
    printd("Left = ");
    printd(xReading, DEC);
    printlnd();
		unsigned int rightReading = analogRead(Y_AXIS);
    printd("Right = ");
    printd(rightReading, DEC);
    printlnd();
		if (pid_was_high_left && xReading == pid_x_neutral) {
			pid_number_of_move_left += 1;
      printlnd("Move left stored");
			pid_reset();
		} else if (pid_was_high_right && xReading == pid_x_neutral) {
      printlnd("Move right stored");
			pid_number_of_move_right += 1;
			pid_reset();
		} else if (pid_was_high_up && rightReading == pid_y_neutral) {
      printlnd("Move up stored");
			pid_number_of_move_up += 1;
			pid_reset();
		} else if (pid_was_high_down && rightReading == pid_y_neutral) {
      printlnd("Move down stored");
			pid_number_of_move_up = 0;
			pid_reset();
		} else { // new move
			if (xReading < pid_lowerThreshold) {
        printlnd("X reading right");
				pid_was_high_right = true;
			} else if (xReading > pid_highThreshold) {
        printlnd("X reading left");
				pid_was_high_left = true;
			} else if (rightReading > pid_highThreshold) {
        printlnd("Y reading down");
				pid_was_high_down = true;
			} else if (rightReading < pid_lowerThreshold) {
        printlnd("Y reading up");
				pid_was_high_up = true;
			} else {
        printlnd("No reading...");
			}
		}
		return NULL;
	}
}


