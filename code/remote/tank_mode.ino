#include "tank_mode.h"
#include "debug.h"
#include <VirtualWire.h>

int tank_leftZero, tank_rightZero;
uint8_t tank_controller[3];

int normalize(int value, int zero) {
	if (value >= zero) {
		value = value - zero;
    value = value / 2;
    if (value > 255)
      value = 255;
    return value;
	} else {
    value = zero - value;
    value = value / 2;
    if (value > 255)
      value = 255;
    return value;
	}
}

void tank_setup() {
	tank_leftZero = analogRead(LEFT);
  printd("Left zero = ");
  printlnd(tank_leftZero);
	tank_rightZero = analogRead(RIGHT);
  printd("Right zero = ");
  printlnd(tank_rightZero);
}

void buildMessage(unsigned int leftCurrent, unsigned int rightCurrent) {
  tank_controller[0] = 0;
  if (leftCurrent < tank_leftZero) {
    tank_controller[0] = tank_controller[0] | 1;
  }
  if (rightCurrent < tank_rightZero) {
    tank_controller[0] = tank_controller[0] | 0b10;
  }
  tank_controller[1] = normalize(leftCurrent, tank_leftZero);
  tank_controller[2] = normalize(rightCurrent, tank_rightZero);
}

uint8_t* tank_get_message() {
	printlnd("Step----");
	int leftCurrent = analogRead(LEFT);
	int rightCurrent = analogRead(RIGHT);
  printd("Read left power: ");
  printlnd(leftCurrent);
  printd("Read right power: ");
  printlnd(rightCurrent);
  buildMessage(leftCurrent, rightCurrent);
  printlnd("---------MESSAGE---------");
  printlnd(tank_controller[0], BIN);
  printlnd(tank_controller[1], DEC);
  printlnd(tank_controller[2], DEC);
  printlnd();
	return tank_controller;
}
