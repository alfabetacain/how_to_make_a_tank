#ifndef __RADIO_COMMS_H_
#define __RADIO_COMMS_H_

#include <VirtualWire.h>
#include "motorControls.h"

const byte rfReceivePin = 4;  //RF Receiver pin


void setupRadioComms(void);
bool receiveCommand(MotorControls* command, bool* pidMode);


#endif
