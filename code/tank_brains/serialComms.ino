#include "serialComms.h"

#define SERIAL_PREAMBLE 0b10101010

byte serialBuff[6];

void setupSerialComms(void) {
  Serial1.begin(345600);
  serialBuff[0] = serialBuff[1] = serialBuff[2] = SERIAL_PREAMBLE;
}

void sendMotorControls(MotorControls* command) {
  serialBuff[3] = (command->a.forward ? 1 : 0) | ((command->b.forward ? 1 : 0) << 1);
  serialBuff[4] = command->a.velocity;
  serialBuff[5] = command->b.velocity;

  Serial1.write(serialBuff, 6);
  Serial1.flush();
}

