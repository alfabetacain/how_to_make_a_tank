#include "radioComms.h"

uint8_t BUFLEN = 3;
uint8_t buf[3];

uint8_t lastPidId;

void setupRadioComms(void) {
  pinMode(rfReceivePin, INPUT);
  vw_set_ptt_inverted(true);
  vw_set_rx_pin(rfReceivePin);
  vw_setup(1000);
  vw_rx_start();
}

bool receiveCommand(MotorControls* response) {
  if (vw_have_message()) {
    BUFLEN = 3;
    if (vw_get_message(buf, &BUFLEN)) {
      // read direction from first bit of buf[0]
      response->a.forward = (buf[0] & 1) == 1;
      response->a.velocity = buf[1];
      
      // read direction from second bit of buf[0]
      response->b.forward = (buf[0] & 0b10) == 0b10;
      response->b.velocity = buf[2];

      response->pidMode = (buf[0] & 0b100) == 0b100;

      if (response->pidMode) {
        if (lastPidId == buf[0]) {
          // repeated message
          return false;
        }
        lastPidId = buf[0];
      }
      
      return true;
    }
  }

  return false;
}

