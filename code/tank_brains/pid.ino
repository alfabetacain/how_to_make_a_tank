#include "motorControls.h"
#include <PID_v1.h>

double Setpoint, Input, Output;
double setHeading;

PID myPID(&Input, &Output, &Setpoint,1,0,1, DIRECT);

void setupPid()
{
  setupCompass();
  setHeading = 230;
  Input = setHeading - getHeading();
  Setpoint = 0;

  myPID.SetOutputLimits(-512, 511);
  myPID.SetSampleTime(1000);
//  myPID.SetInputLimits(0, 360);

  //turn the PID on
  myPID.SetMode(AUTOMATIC);
}

void setDeltaHeading(double heading) {
  float current = getHeading();
  setHeading = heading + current;
  if (setHeading > 180) setHeading -= 360;
  else if (setHeading < -180) setHeading += 360;
}

int pid() {
  float heading = getHeading();
  Input = (setHeading - heading);
  if (Input > 180) Input -= 360;
  else if (Input < -180) Input += 360;

  if (myPID.Compute()) {
    Serial.print("I: "); Serial.print(Input);
    Serial.print(" O: "); Serial.println(Output);
    return Output;
  }

  return -1000;
}

void applyPid(MotorControls* command) {
  int p = pid();
  if (p != -1000) {
    command->a.forward = true;
    command->b.forward = true;
    command->a.velocity = 255;
    command->b.velocity = 255;
  
    if (p < 0) {
      if (p < -255) {
        command->a.forward = false;
        command->a.velocity = p + 511;
      } else {
        command->a.velocity = p + 255;
      }
    } else {
      if (p > 255) {
        command->b.forward = false;
        command->b.velocity = p - 256;
      } else {
        command->b.velocity = p;
      }
    }
  }
}

