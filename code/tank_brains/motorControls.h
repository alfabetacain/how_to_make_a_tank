#ifndef __MOTOR_CONTROLS_H__
#define __MOTOR_CONTROLS_H__

typedef struct motor_control {
  bool forward;
  uint8_t velocity;
} MotorControl;

typedef struct motor_controls {
  // Controls the meaning of other parameters in the MotorControl struct.
  // If set to false, forward denotes the diraction, and velocity denotes the amount of power
  // that should be sent to the DC-motor. If set to true,   
  bool pidMode;
  MotorControl a;
  MotorControl b;
} MotorControls;

#endif
