#include "ultrasound.h"

const byte FORWARD_PIN = 6;
const byte BACKWARD_PIN = 7;

const byte MAX_DISTANCE = 25;
const byte DISTANCE_TRESHOLD = 20;

NewPing sonar[2] = {
  NewPing(FORWARD_PIN, FORWARD_PIN, MAX_DISTANCE),
  NewPing(BACKWARD_PIN, BACKWARD_PIN, MAX_DISTANCE)
};

bool shouldStop(bool forward) {
  int d = sonar[forward ? 0 : 1].ping_cm();
  return d == 0 ? false : d < DISTANCE_TRESHOLD;
}

void shouldStop(MotorControls* command) {
  bool forward, backward;

  if (command->a.forward) {
    forward = true;
  } else {
    backward = true;
  }
  if (command->b.forward) {
    forward = true;
  } else {
    backward = true;
  }

  if (forward) {
    if (shouldStop(true)) {
      Serial.println("Stopping forward movement");
      if (command->a.forward) {
        command->a.velocity = 0;
      }
      if (command->b.forward) {
        command->b.velocity = 0;
      }
    }
  }
  if (backward) {
    if (shouldStop(false)) {
      Serial.println("Stopping backward movement");
      if (!command->a.forward) {
        command->a.velocity = 0;
      }
      if (!command->b.forward) {
        command->b.velocity = 0;
      }
    }
  }
}

