#include <Adafruit_Sensor.h>
#include <Adafruit_FXOS8700.h>

// Create sensor instance.
Adafruit_FXOS8700 accelmag = Adafruit_FXOS8700(0x8700A, 0x8700B);

// Mag calibration values are calculated via ahrs_calibration.
// These values must be determined for each baord/environment.
// See the image in this sketch folder for the values used
// below.

// Offsets applied to raw x/y/z mag values
const float mag_offsets[3]            = { 20.79F, 3.67F, 37.62F };

// Soft iron error compensation matrix
const float mag_softiron_matrix[3][3] = { {  0.924F,  -0.035F,  -0.009F },
                                    {  -0.035F,  0.936F, -0.004F },
                                    {  -0.009F, -0.004F,  1.159F } };

const float mag_field_strength        = 51.11F;

inline void setupCompass()
{
  accelmag.begin(ACCEL_RANGE_4G);
}

float getHeading() {
  sensors_event_t accel_event;
  sensors_event_t mag_event;

  // Get new data samples
  accelmag.getEvent(&accel_event, &mag_event);

  // Apply mag offset compensation (base values in uTesla)
  float x = mag_event.magnetic.x - mag_offsets[0];
  float y = mag_event.magnetic.y - mag_offsets[1];
  float z = mag_event.magnetic.z - mag_offsets[2];

  // Apply mag soft iron error compensation
  float mx = x * mag_softiron_matrix[0][0] + y * mag_softiron_matrix[0][1] + z * mag_softiron_matrix[0][2];
  float my = x * mag_softiron_matrix[1][0] + y * mag_softiron_matrix[1][1] + z * mag_softiron_matrix[1][2];

  float deg = atan2(my, mx) * 180 / PI;

  if (deg < 0) {
    return 360 + deg;
  }

  return deg;
}
