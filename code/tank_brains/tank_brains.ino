#include "motorControls.h"
#include "radioComms.h"
#include "serialComms.h"

const byte buzzerPin = 10;

void setup() {
  setupSerialComms();
  setupRadioComms();
  setupPid();

  pinMode(buzzerPin, OUTPUT);
  digitalWrite(buzzerPin, 0);
}

MotorControls command = { false, {false, 0}, {false, 0} };
const MotorControls stopCommand = { false, {false, 0}, {false, 0} };

long lastIncomingMessage;
long lastPid;
long lastMotor;
long pidUntil;

void loop() {
  long t = millis();
  if (receiveCommand(&command)) {
    lastIncomingMessage = t;
    if (command.pidMode) {
      Serial.println("PidMode");
      tone(buzzerPin, 872, 100);
      setDeltaHeading(command.a.forward ? command.a.velocity * 10 : -command.a.velocity * 10);
      pidUntil = t + 2000 * command.b.velocity; // 2 seconds per forward tab from now.
    } else {
      Serial.println("Not pid mode");
    }
  } else if ((!command.pidMode && t - lastIncomingMessage > 3000) || (command.pidMode && t > pidUntil)) {
    command.pidMode = false;
    lastIncomingMessage = t;
    tone(buzzerPin, 512, 100);
    command.a.velocity = 0;
    command.b.velocity = 0;
    sendMotorControls(&command);
  }
  if (command.pidMode && t - lastPid > 500) {
    lastPid = t;
    applyPid(&command);
  }

  if (t - lastMotor > 100) {
    lastMotor = t;
    shouldStop(&command);

    Serial.print("A: ("); Serial.print(command.a.forward); Serial.print(", "); Serial.print(command.a.velocity); Serial.println(")");
    Serial.print("B: ("); Serial.print(command.b.forward); Serial.print(", "); Serial.print(command.b.velocity); Serial.println(")");
    sendMotorControls(&command);
  }
}
