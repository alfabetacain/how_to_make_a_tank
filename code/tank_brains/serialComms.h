#ifndef __SERIAL_COMMS_H__
#define __SERIAL_COMMS_H__

#include "motorControls.h"

void setupSerialComms(void);
void sendMotorControls(MotorControls* command);

#endif
