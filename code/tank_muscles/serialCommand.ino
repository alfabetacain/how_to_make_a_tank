#include "serialCommand.h"

byte buf[1];

#define SERIAL_PREAMBLE 0b10101010
byte preambleCount = 0;

bool receiveCommand(MotorControls* response) {
  while (Serial1.available()) {
    if (Serial1.readBytes(buf, 1) == 1) {
      if (preambleCount < 3) {
        if (buf[0] == SERIAL_PREAMBLE) {
          preambleCount++;
        }
      } else {
        response->a.forward = (buf[0] & 1) == 1;
        response->b.forward = (buf[0] & 0b10) == 0b10;

        Serial1.readBytes(&(response->a.velocity), 1);
        Serial1.readBytes(&(response->b.velocity), 1);

        preambleCount = 0;
        return true;
      }
    }
  }
  
  return false;
}
