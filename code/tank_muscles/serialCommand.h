#ifndef __SERIAL_COMMAND_H
#define __SERIAL_COMMAND_H

typedef struct motor_control {
  bool forward;
  uint8_t velocity;
} MotorControl;

typedef struct motor_controls {
  MotorControl a;
  MotorControl b;
} MotorControls;

bool receiveCommand(MotorControls* response);

#endif
