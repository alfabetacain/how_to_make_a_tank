#include "serialCommand.h"

const byte speedPinA = 3;
const byte brakePinA = 9;
const byte dirPinA = 12;
const byte speedPinB = 11;
const byte brakePinB = 8;
const byte dirPinB = 13;


void setup() {
  Serial.begin(9600);
  Serial1.begin(345600);

  pinMode(dirPinA, OUTPUT);
  pinMode(speedPinA, OUTPUT);
  pinMode(brakePinA, OUTPUT);
  digitalWrite(brakePinA, LOW);
  
  pinMode(dirPinB, OUTPUT);
  pinMode(speedPinB, OUTPUT);
  pinMode(brakePinB, OUTPUT);
  digitalWrite(brakePinB, LOW);
}

void setMotorSpeed(byte dirPin, byte speedPin, MotorControl* mc) {
  digitalWrite(dirPin, mc->forward ? HIGH : LOW);
  analogWrite(speedPin, mc->velocity);
}

MotorControls command;

void loop() {
  if (receiveCommand(&command)) {
    Serial.println("Command received");
    Serial.print("A: ("); Serial.print(command.a.forward); Serial.print(", "); Serial.print(command.a.velocity); Serial.println(")");
    Serial.print("B: ("); Serial.print(command.b.forward); Serial.print(", "); Serial.print(command.b.velocity); Serial.println(")");
    
    setMotorSpeed(dirPinA, speedPinA, &command.a);
    setMotorSpeed(dirPinB, speedPinB, &command.b);
  }
}


