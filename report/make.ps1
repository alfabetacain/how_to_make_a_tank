param(
	[switch]$Clean,
	[string]$auxDir = "$psScriptRoot/aux_dir",
	[string]$pdfName = 'report'
)
if (-not (Test-Path $auxDir)) {
	mkdir $auxDir
}
if ($Clean) {
	Remove-Item -Recurse -Force "$auxDir\*"
	Remove-Item "$psScriptRoot\$pdfName.pdf"
} else {
	pdflatex -aux-directory "$auxDir" -quiet -interaction=batchmode -job-name "$pdfName" "$psScriptRoot\how_to_make_a_tank.tex"
}
