% !TEX root = ../how_to_make_a_tank.tex
% !TeX spellcheck = en_GB
\section{Description}
%How does the device work? Describe in as much detail as you can fit into the report
%It should contain 3 subsections: Mechanics, Electronics, Software/Firmware.
%Describe also what alternatives you analyzed for the different parts of your device.
%Why did you select the alternative that you finally used?

The project consists of two major components, the tank and the remote, and has two operating modes: tank mode and PID mode. Pressing the left joystick will switch between modes.

Tank mode is, as the name implies, meant to closely resemble the old fashioned way of driving a tank; namely with two levers, each controlling forward/backward movement of one of the tracks. Therefore, in this mode each joystick will respond to being moved up and down along the y-axis and transmit this information to the tank, moving it correspondingly forward or backwards. When in tank mode, the green LED on the remote will be lit.

PID mode has a less direct way of controlling the tank. In this mode joystick movements are recorded rather than being relayed immediately. Moving the right joystick to the left will move it a fixed amount of degrees counter clockwise and correspondingly moving it to the right moves it the same amount clockwise. Moving the joystick up will increase the time the tank moves forward before stopping, and moving it down will reset this time.
Pressing the right joystick will transmit the instructions to the tank. When in PID mode, the red LED on the remote is lit instead of the green LED.

The tank is built from two Arduinos. One for receiving commands from the remote, and one to control the DC motors. Since we are using Arduino Leonardos for the tank, a single Leonardo was not able to use the I2C protocol while controlling motors through the motor shield, because the pins overlap. We could have replaced the Leonardo with an Uno, but we were also having problems with powering all components, so we decided to use two Leonardos instead -- one for powering the motors (referred to as "Muscles") and the other for communication and sensors (referred to as "Brains").

Stepper motors would have been an option as well, but since we do not have the need for turning a specific amount of degrees or steps on the wheels, we stuck to the cheaper and smaller DC motors.

For communication we only have the need for sending commands to the vehicle. Therefore we looked at radio communication, using a transmitter on the remote and a receiver on the tank.

The tank consists of the following electronic and electrical parts:
\begin{itemize}
	\item Two Arduino Leonardos
	\item A radio receiver
	\item A combinded gyro, accellerometer, and magnetometer
	\item Two ultrasonic sensors, used to avoid hitting obstacles
	\item A piezo-speaker that warns the user when no commands are transfered
	\item An Arduino Motor Shield
	\item Two DC Motors
	\item An assortment of wires, connecting the components
	%\item batteri pack? 
\end{itemize}

The tank consist of the following non-electronic parts:
\begin{itemize}
	\item An acrylic resting plate (referred to as the body), see specifications in annexes, figure \ref{fig:tank-body}
	\item Two PLA front track hubs, see specifications in annexes, figure \ref{fig:hub-front}
	\item Two PLA back track hubs, see specifications in annexes, figure \ref{fig:hub-back}
	\item Four Ball Bearing 625 2RS 5x16x5 (from a Delrin V Wheel Kit \cite{wheel-kit}) 
	\item A 23cm M5 threaded rod, which serves as an axle for the back track hubs
	\item Four snapfit POM axle fixtures, see specifications in annexes, figure \ref{fig:axle-fixture}
	\item Two Ecoflex\textregistered{} 00-50 casted continuous tracks, see specifications in annexes, figure \ref{fig:track} 
	\item An assortment of screws and bolts securing the parts
\end{itemize}

The remote consists of the following electronic and electrical parts: 
\begin{itemize}
	\item An Arduino, acting as a control unit
	\item Two joystick sensors
	\item A radio transmitter
	\item A voltage booster
	\item Two LEDs, to make the remote more flashy
	\item An assortment of wires connecting the components
\end{itemize}

The remote consist of the following non-electronic parts:
\begin{itemize}
	\item An acrylic resting plate, referred to as the remote body, see specifications in annexes, figure \ref{fig:remote-drawing}
	\item An assortment of screws and bolts securing the parts
\end{itemize}

\subsection{Mechanics}
The only mechanical parts of the tank are the track hubs (wheels, if you will) and the track itself.

The two front track hubs are attached to a DC motor each.
The engines are powered through an Arduino motor-shield, which will regulate the power that is sent to each wheel according to the mode in which the tank is running (more on this in section \ref{subsec:electronics} and \ref{subsec:software}).

The front hubs are attached to the motors through pure friction - the sockets in track hubs for the motor shaft is designed to be so tight that the track hubs are securely fastened, unless some force is applied to the hubs in a direction away from the tank body.

On the outside surface of the track hubs, the tracks rest. 
The tracks are made from soft silicone (Ecoflex\textregistered{} 00-50 \cite{ecoflex}) and are kept taut by the back track hubs.
The silicone tracks provide a lot of friction, allowing the rotational movement of the hubs to be transferred to the tracks, which in turn translates into forward movement through friction with the surface the tracks are resting on.

The tracks are prevented from falling of the track hubs by 7 mm flanges on the wheel hubs.
The flanges are at an 135\textdegree{} angle from the face that the inner side of the tracks are resting on.
If the tracks begin sliding onto the flanges, the part of the track on the flange will expand, leading this part of the track to travel a longer distance than the part still resting on the center of the hub.
This will force the track back in place. 

\subsection{Electronics}
\label{subsec:electronics}

\subsubsection{Tank}
The tank schematic can be found in \autoref{fig:tank_schematic} in the annexes. Starting with "Brains", it consists of an Arduino Leonardo, connected to all the sensors, the speaker, the radio-receiver and a serial connection to the other Arduino.

The ultrasonic sensors have four pins. Two pins for power, one trigger pin, and one echo pin. The trigger pin sends the signal, while the echo pin reports the response. Looking carefully at the documentation for the NewPing library \cite{newping}, it can be seen that these pins can be connected to the same digital pin on the Arduino, and the library  will then take care of signal and response.

The piezo buzzer is connected to ground, and its other pin is connected to a digital pin with pulse width modulation. This way we can control the pitch of the buzzer to signal different events.

The combined accelerometer, magnetometer, and gyro (unused) is connected to the Arduinos I2C pins. This is necessary since the same board contains multiple sensors. The Adafruit Sensor libraries\cite{adafruit-sensor-library} take care of the communication with these devices.

On the Arduino Leonardo the I2C protocol uses digital pins 2 and 3, which unfortunately conflicts with the motor shield, leaving us with two choices: Either we could use an Arduino Uno, or we could split up the tank into two Leonardos. Since we also had power issues, we went with the two-Leonardo-solution.

The radio receiver has a power circuit and two data pins. It is to this day unknown why it has two data pins, and they seem equivalent in functionality. We have chosen to use the one closest to the VCC-port.

Since we have to communicate between two Arduinos, we have to connect them between the transmitter's TX-port and the receiver's RX-port. Also, they need to have a common ground such that data can be read reliably.

The "Muscles" is, besides from the serial connection and the common ground, only connected to the DC motors using the motor shield.

Other than these connections, each Arduino has its own power supply. "Muscles" needs higher voltage than "Brains", and therefore it is connected to a 9V battery.

\subsubsection{Remote}

The remote schematic can be found in \autoref{fig:remote_schematic} in the annexes. It consists of an Arduino Uno, two joysticks, a radio transmitter, and a step up voltage converter. 

The two joysticks each provide three data pins: two analog pins for x and y movement, and one digital pin for pressing the joystick like a button. Note that the resting point for the x/y movements are not 511/512 as one would expect, but is usually between the larger span of 505 and 525. 

The transmitter uses only a single digital pin besides power pins. This pin is controlled by the VirtualWire library \cite{virtualwire} and is connected directly into a digital port on the Uno. 
The power connections do not go directly into the Uno, but goes through a voltage step up converter, which "steps up" the voltage from about 5 volts to about 11 volts, in order to improve the range of the transmitter. The voltage converter is then connected to the Uno for power. 

\subsection{Software/Firmware}
\label{subsec:software}

As mentioned, the tank consists of two components: "Brains" and "Muscles".

"Brains" receives commands over the radio components from the remote using the \textbf{VirtualWire} library \cite{virtualwire}. Each command consists of three bytes that are interpreted differently based on the mode.

\begin{itemize}
	\item Tank mode
	\begin{itemize}
		\item In the first byte, the third bit is set to zero to denote tank mode. The first and second bits denote the direction of the left and right motor respectively.
		\item The second and third bytes denote the power given to the left and right motor respectively.
	\end{itemize}
	\item PID mode
	\begin{itemize}
		\item In the first byte, the third bit is set to one to denote PID mode. The first bit denotes the sign of the second byte. Furthermore, since there is no continuous transmission of commands in PID mode, and we have no guarantees on delivery, the last four bits of the first byte denotes a number identifying the command, such that we can transmit it multiple times, without confusing the tank.
		\item The second byte denotes the number of "steps" that the tank should turn when driving. Note that the sign, deciding the direction to turn was given in the first byte.
		\item The third byte denotes the amount of time that the tank should move. % TODO: we should probably be more specific about this. Also, what does negative mean in terms of the last byte?
	\end{itemize}
\end{itemize}

In tank mode, "Brains" will just forward the command to "Muscles", after checking that the tank is not too close to objects in the direction it is driving.

If the tank is in tank mode, and does not receive instructions for three seconds, "Brains" will send a buzz command to the piezo buzzer, to warn the user. 

Because the magnetometer does not directly report a heading, but rather "magnetic field strengths" on each of the axes, some calibration and calculations has to be done in order to get a heading. Note that this heading does not necessarily point to north at 0\textdegree{}.

A PID controller uses a set point and continuous sensor input to minimize the error between these values. We use PID to make the tank turn to, and follow, a specific heading.
When using PID, "Brains" sets a heading relative to the current, and tells the PID to aim for this. Its output is the power difference between the left and the right motor.  

To synchronize communication between "Brains" and "Muscles", each message is prefixed with 3 bytes of "10101010".


\subsection{Methods}
For the construction of the tank, we have used the following construction methods, learned trough the course of the lessons:\\
\begin{itemize}
	\item Silicone casting for the tracks
	\item Laser cutting for the two bodies, and the axle fixtures
	\item 3d-printing for the track hubs
	\item Soldering for connecting the wires to a copper PCB-prototype
	\item Modelling the parts in Fusion 360
	\item Embedded programming for programming the Arduinos
\end{itemize}

We have had multiple iterations of several components.
We started by designing track hubs as wheel equivalents, with very thin and short flanges, and deep depressions in the track-resting area. The depressions served to "grab" the elevations in the track, and push it forward.
After testing, this approach turned out to be completely misjudged.
The flanges were insufficient, and the depressions unnecessary, due to the enormous friction provided by the track.
Thus, the second (and current) iteration focus on larger and wider flanges, and no depressions.

The snapfit fixtures has been through four(!) iterations.
It turns out that snapfit need a very high precision in its measurements, and should be made of a more flexible material than acrylic. 
The latest design, which can be seen in figure \ref{fig:axle-fixture} in annexes, is still not perfect, as it is quite hard to fit into the tank body, but it is usable.
The three first iterations can be seen in figure \ref{fig:fixture-its}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.3\textwidth]{files/snapfits1to3.jpg}
	\caption{The three first iterations of the snapfit fixtures \label{fig:fixture-its}}
\end{figure}

The track has kept its design, but have been the subject of much speculation and trial-and-error.
We initially wanted to print it in Ecoflex\textregistered{}, using a 3D-printer, but it turned out to be very cumbersome to adjust the printer correctly for this material, and we never got it to work.
Instead we cast it, adding nylon thread to prevent the circumference from expanding.
This was a somewhat viable solution. 
