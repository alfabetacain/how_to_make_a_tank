# How to make a tank!

## Protocol

### Sane mode
<pre>
3 bytes

first byte
1st bit = direction left (1 = forward)
2nd bit = direction right (1 = forward)
3rd bit = 0

second byte = velocity for left

third byte = velocity for right
</pre>

### Stig mode
<pre>
first byte:
	- 1st bit = turn direction (left = 1, right = 0)
	- 2nd bit = move direction (down = 1, up = 0)
	- 3rd bit = 1

second byte = number of turns
third byte = number of moves
</pre>

## Remote pin setup

<pre>
LEFT = A0
RIGHT = A1
SWITCH_MODE_PIN = 2
STIG_MODE_SEND_PIN = 3
TRANSMISSION_PIN = 4
</pre>


## VirtualWire library

See lib folder and this link https://www.pjrc.com/teensy/td_libs_VirtualWire.html
